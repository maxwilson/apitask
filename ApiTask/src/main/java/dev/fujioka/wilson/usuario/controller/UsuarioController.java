/**
 * 
 */
package dev.fujioka.wilson.usuario.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.fujioka.wilson.usuario.Usuario;
import dev.fujioka.wilson.usuario.service.UsuarioService;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class UsuarioController {
	
	@Autowired
	private UsuarioService service;
	

	@Autowired
	public UsuarioController(UsuarioService service) {
		this.service = service;
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/usuarios")
	public ResponseEntity<Usuario> save(@RequestBody Usuario usuario) {
		
		// data local
		LocalDate datalocal = LocalDate.now();
		usuario.setCreate_at(datalocal);
		
		this.service.save(usuario);
		return new ResponseEntity<Usuario>(usuario, HttpStatus.CREATED);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/usuarios")
	public ResponseEntity<List<Usuario>> list() {
		List<Usuario> usuario = this.service.list();
		return ResponseEntity.ok(this.service.list());
	}
	
	
	@ResponseStatus(code = HttpStatus.FOUND)
	@GetMapping("/usuarios/{id}")
	public ResponseEntity<Usuario> get(@PathVariable Long id){
		
		Optional<Usuario> usuarioOptional = this.service.getById(id);
		
		if(usuarioOptional.isPresent()) {
			Usuario usuario = usuarioOptional.get();
			return ResponseEntity.ok().body(usuario);
		}
		return ResponseEntity.noContent().build();
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@DeleteMapping("/usuarios/{id}")
	public ResponseEntity<Usuario> delete(@PathVariable Long id) {
		Optional<Usuario> usuario = this.service.getById(id);
		if (usuario.isPresent()) {
			this.service.deleteById((usuario.get().getId()));
			return ResponseEntity.ok().body(usuario.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping("/usuarios/edit")
	public ResponseEntity<Usuario> edit(@RequestBody Usuario usuario){
		Optional<Usuario> usuarioOptional = this.service.getById(usuario.getId());
		Usuario updateUsuario = usuarioOptional.get();
		if(usuarioOptional.isPresent()) {
			// data local
			LocalDate datalocal = LocalDate.now();
			updateUsuario.setNome(usuario.getNome());
			updateUsuario.setEmail(usuario.getEmail());
			updateUsuario.setTelefone(usuario.getTelefone());
			updateUsuario.setUpdate_at(datalocal);
			this.service.save(updateUsuario);		
			return ResponseEntity.ok(updateUsuario);
		}else {
			return ResponseEntity.noContent().build();
		}
		
	}

}
