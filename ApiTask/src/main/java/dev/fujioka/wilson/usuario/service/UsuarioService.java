/**
 * 
 */
package dev.fujioka.wilson.usuario.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.fujioka.wilson.usuario.Usuario;
import dev.fujioka.wilson.usuario.repository.UsuarioRepository;

/**
 * @author wilson
 *
 */
@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	@Transactional
	public void save(Usuario usuario) {
		this.repository.save(usuario);
	}
	
	@Transactional
    public void delete(Usuario entity) {
		repository.delete(entity);
    }

	@Transactional
    public void deleteById(long id) {
		repository.deleteById(id);
    }
	
	@Transactional(readOnly=true)
	public Optional<Usuario> getById(Long id) {
		return this.repository.findById(id);
	}
	
	@Transactional
    public long count() {
        return repository.count();
    }
    
	@Transactional(readOnly=true)
	public List<Usuario> list() {
		return this.repository.findAll();
	}
}
