/**
 * 
 */
package dev.fujioka.wilson.usuario.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.fujioka.wilson.usuario.Usuario;

/**
 * @author wilson
 *
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
