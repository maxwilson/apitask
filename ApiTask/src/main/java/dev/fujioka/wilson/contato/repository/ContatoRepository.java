/**
 * 
 */
package dev.fujioka.wilson.contato.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.fujioka.wilson.contato.Contato;

/**
 * @author wilson
 *
 */
@Repository
public interface ContatoRepository extends JpaRepository<Contato, Long> {

}
