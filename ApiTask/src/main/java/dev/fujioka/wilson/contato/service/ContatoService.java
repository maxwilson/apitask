/**
 * 
 */
package dev.fujioka.wilson.contato.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.fujioka.wilson.contato.Contato;
import dev.fujioka.wilson.contato.repository.ContatoRepository;

/**
 * @author wilson
 *
 */
@Service
public class ContatoService {
	
	@Autowired
	private ContatoRepository repository;
	
	@Transactional
	public void save(Contato entity) {
		this.repository.save(entity);
	}
	
	@Transactional
    public void delete(Contato entity) {
		repository.delete(entity);
    }

	@Transactional
    public void deleteById(long id) {
		repository.deleteById(id);
    }
	
	@Transactional(readOnly=true)
	public Optional<Contato> getById(Long id) {
		return this.repository.findById(id);
	}
	
	@Transactional
    public long count() {
        return repository.count();
    }
    
	@Transactional(readOnly=true)
	public List<Contato> list() {
		return this.repository.findAll();
	}
}
