/**
 * 
 */
package dev.fujioka.wilson.contato.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.fujioka.wilson.contato.Contato;
import dev.fujioka.wilson.contato.service.ContatoService;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class ContatoController {
	
	@Autowired
	private ContatoService service;
	

	@Autowired
	public ContatoController(ContatoService service) {
		this.service = service;
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/contatos")
	public ResponseEntity<Contato> save(@RequestBody Contato entity) {
		// data local
		LocalDate datalocal = LocalDate.now();
		entity.setCreate_at(datalocal);
		this.service.save(entity);
		return new ResponseEntity<Contato>(entity, HttpStatus.CREATED);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/contatos")
	public ResponseEntity<List<Contato>> list() {
		List<Contato> contatos = this.service.list();
		return ResponseEntity.ok(contatos);
	}
	
	@ResponseStatus(code = HttpStatus.FOUND)
	@GetMapping("/contatos/{id}")
	public ResponseEntity<Contato> get(@PathVariable Long id){
		Optional<Contato> contatoOptional = this.service.getById(id);
		if(contatoOptional.isPresent()) {
			Contato contato = contatoOptional.get();
			return ResponseEntity.ok().body(contato);
		}
		return ResponseEntity.noContent().build();
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@DeleteMapping("/contatos/{id}")
	public ResponseEntity<Contato> delete(@PathVariable Long id) {
		Optional<Contato> contato = this.service.getById(id);
		if (contato.isPresent()) {
			this.service.deleteById((contato.get().getId()));
			return ResponseEntity.ok().body(contato.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping("/contatos")
	public ResponseEntity<Contato> edit(@RequestBody Contato contato){
		Optional<Contato> contatoOptional = this.service.getById(contato.getId());
		Contato updateContato = contatoOptional.get();
		if(contatoOptional.isPresent()) {
			// data local
			LocalDate datalocal = LocalDate.now();
			updateContato.setNome(contato.getNome());
			updateContato.setTelefone(contato.getTelefone());
			updateContato.setUpdate_at(datalocal);
			this.service.save(updateContato);		
			return ResponseEntity.ok(updateContato);
		}else {
			return ResponseEntity.noContent().build();
		}
		
	}
	
}
