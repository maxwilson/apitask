/**
 * 
 */
package dev.fujioka.wilson.categoria.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.fujioka.wilson.categoria.CategoriaTask;
import dev.fujioka.wilson.categoria.service.CategoriaTaskService;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class CategoriaTaskController {
	
	@Autowired
	private CategoriaTaskService service;
	

	@Autowired
	public CategoriaTaskController(CategoriaTaskService service) {
		this.service = service;
	}
	
	//salva categoria
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/task/categorias")
	public ResponseEntity<CategoriaTask> save(@RequestBody CategoriaTask categoria) {
		// data local
		LocalDate datalocal = LocalDate.now();
		categoria.setCreate_at(datalocal);
		this.service.save(categoria);
		return new ResponseEntity<CategoriaTask>(categoria, HttpStatus.CREATED);
	}
	
	// retorna lista de categorias cadastradas
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/task/categorias")
	public ResponseEntity<List<CategoriaTask>> list() {
		List<CategoriaTask> categorias = this.service.list();
		return ResponseEntity.ok(categorias);
	}
	
	//Solicita categoria por ID
	@ResponseStatus(code = HttpStatus.FOUND)
	@GetMapping("/task/categorias/{id}")
	public ResponseEntity<CategoriaTask> get(@PathVariable Long id){
		Optional<CategoriaTask> categoriaOptional = this.service.getById(id);
		if(categoriaOptional.isPresent()) {
			CategoriaTask categoria = categoriaOptional.get();
			return ResponseEntity.ok().body(categoria);
		}
		return ResponseEntity.noContent().build();
	}
	
	//deleta categoria HHTPS do tipo DELETE
	@ResponseStatus(code = HttpStatus.OK)
	@DeleteMapping("/task/categorias/{id}")
	public ResponseEntity<CategoriaTask> delete(@PathVariable Long id) {
		Optional<CategoriaTask> categoria = this.service.getById(id);
		if (categoria.isPresent()) {
			this.service.deleteById((categoria.get().getId()));
			return ResponseEntity.ok().body(categoria.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}
	
	//Update categoria via PUT
	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping("/task/categorias")
	public ResponseEntity<CategoriaTask> edit(@RequestBody CategoriaTask categoria){
		Optional<CategoriaTask> categoriaOptional = this.service.getById(categoria.getId());
		CategoriaTask updateCategoria = categoriaOptional.get();
		if(categoriaOptional.isPresent()) {
			// data local
			LocalDate datalocal = LocalDate.now();
			updateCategoria.setDescricao(categoria.getDescricao());
			updateCategoria.setPrioridade(categoria.isPrioridade());
			updateCategoria.setUpdate_at(datalocal);
			
			this.service.save(updateCategoria);		
			return ResponseEntity.ok(updateCategoria);
		}else {
			return ResponseEntity.noContent().build();
		}
		
	}
	
	
	
}
