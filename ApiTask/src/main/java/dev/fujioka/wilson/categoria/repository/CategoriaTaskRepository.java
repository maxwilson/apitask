/**
 * 
 */
package dev.fujioka.wilson.categoria.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.fujioka.wilson.categoria.CategoriaTask;

/**
 * @author wilson
 *
 */
@Repository
public interface CategoriaTaskRepository extends JpaRepository<CategoriaTask, Long> {

}
