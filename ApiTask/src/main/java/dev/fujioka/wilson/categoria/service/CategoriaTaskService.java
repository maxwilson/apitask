/**
 * 
 */
package dev.fujioka.wilson.categoria.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.fujioka.wilson.categoria.CategoriaTask;
import dev.fujioka.wilson.categoria.repository.CategoriaTaskRepository;

/**
 * @author wilson
 *
 */
@Service
public class CategoriaTaskService {
	
	@Autowired
	private CategoriaTaskRepository repository;
	
	@Transactional
	public void save(CategoriaTask categoria) {
		this.repository.save(categoria);
	}
	
	@Transactional
    public void delete(CategoriaTask categoria) {
		repository.delete(categoria);
    }

	@Transactional
    public void deleteById(long id) {
		repository.deleteById(id);
    }
	
	@Transactional(readOnly=true)
	public Optional<CategoriaTask> getById(Long id) {
		return this.repository.findById(id);
	}
	
	@Transactional
    public long count() {
        return repository.count();
    }
    
	@Transactional(readOnly=true)
	public List<CategoriaTask> list() {
		return this.repository.findAll();
	}
}
