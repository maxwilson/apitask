/**
 * 
 */
package dev.fujioka.wilson.agendamento.controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.fujioka.wilson.agendamento.Agendamento;
import dev.fujioka.wilson.agendamento.service.AgendamentoService;
import dev.fujioka.wilson.task.Task;

/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class AgendamentoController {
	
	@Autowired
	private AgendamentoService service;
	

	@Autowired
	public AgendamentoController(AgendamentoService service) {
		this.service = service;
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/agendamentos")
	public ResponseEntity<Agendamento> save(@RequestBody Agendamento agendamento,@RequestParam(name = "contato_id") Long contato_id,
			@RequestParam(name = "usuario_id") Long usuario_id) {
		
		// data local
		LocalDate datalocal = LocalDate.now();
		agendamento.setCreate_at(datalocal);
		
		this.service.save(agendamento,contato_id,usuario_id);
		return new ResponseEntity<Agendamento>(agendamento, HttpStatus.CREATED);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/agendamentos")
	public ResponseEntity<List<Agendamento>> list() {
		List<Agendamento> agendamentos = this.service.list();
		return ResponseEntity.ok(agendamentos);
	}
	
	@ResponseStatus(code = HttpStatus.FOUND)
	@GetMapping("/agendamentos/{id}")
	public ResponseEntity<Agendamento> get(@PathVariable Long id){
		
		Optional<Agendamento> agendamentosOptional = this.service.getById(id);
		
		if(agendamentosOptional.isPresent()) {
			Agendamento agendamentos = agendamentosOptional.get();
			return ResponseEntity.ok().body(agendamentos);
		}
		return ResponseEntity.noContent().build();
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@DeleteMapping("/agendamentos/{id}")
	public ResponseEntity<Agendamento> delete(@PathVariable Long id) {
		Optional<Agendamento> agendamento = this.service.getById(id);
		if (agendamento.isPresent()) {
			this.service.deleteById((agendamento.get().getId()));
			return ResponseEntity.ok().body(agendamento.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping("/agendamentos")
	public ResponseEntity<Agendamento> edit(@RequestBody Agendamento agendamento,@RequestParam(name = "contato_id") Long contato_id,
			@RequestParam(name = "usuario_id") Long usuario_id){
		
		Optional<Agendamento> agendamentoOptional = this.service.getById(agendamento.getId());
		Agendamento updateAgendamento = agendamentoOptional.get();
		
		if(agendamentoOptional.isPresent()) {
			// data local
			LocalDate datalocal = LocalDate.now();
			updateAgendamento.setTitulo(agendamento.getTitulo());
			updateAgendamento.setDescricao(agendamento.getDescricao());
			updateAgendamento.setData(agendamento.getData());
			updateAgendamento.setHora(agendamento.getHora());
			updateAgendamento.setVigente(agendamento.isVigente());
			updateAgendamento.setUpdate_at(datalocal);
			this.service.save(updateAgendamento,contato_id,usuario_id);		
			return ResponseEntity.ok(updateAgendamento);
		}else {
			return ResponseEntity.noContent().build();
		}
		
	}
	
}
