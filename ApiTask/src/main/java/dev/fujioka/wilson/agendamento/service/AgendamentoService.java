/**
 * 
 */
package dev.fujioka.wilson.agendamento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.fujioka.wilson.agendamento.Agendamento;
import dev.fujioka.wilson.agendamento.repository.AgendamentoRepository;
import dev.fujioka.wilson.contato.Contato;
import dev.fujioka.wilson.contato.repository.ContatoRepository;
import dev.fujioka.wilson.usuario.Usuario;
import dev.fujioka.wilson.usuario.repository.UsuarioRepository;

/**
 * @author wilson
 *
 */
@Service
public class AgendamentoService {
	
	@Autowired
	private AgendamentoRepository repository;
	
	@Autowired
	private UsuarioRepository usuario_repository;
	
	@Autowired
	private ContatoRepository contato_repository;
	
	
	@Transactional
	public void save(Agendamento agendamento, Long contato_id, Long usuario_id) {
		
		Usuario usuario = this.usuario_repository.getOne(usuario_id);
		Contato contato = this.contato_repository.getOne(contato_id);
		
		agendamento.setContato(contato);
		agendamento.setUsuario(usuario);
		this.repository.save(agendamento);
	}
	
	@Transactional
    public void delete(Agendamento entity) {
		repository.delete(entity);
    }

	@Transactional
    public void deleteById(long id) {
		repository.deleteById(id);
    }
	
	@Transactional(readOnly=true)
	public Optional<Agendamento> getById(Long id) {
		return this.repository.findById(id);
	}
	
	@Transactional
    public long count() {
        return repository.count();
    }
    
	@Transactional(readOnly=true)
	public List<Agendamento> list() {
		return this.repository.findAll();
	}
}
