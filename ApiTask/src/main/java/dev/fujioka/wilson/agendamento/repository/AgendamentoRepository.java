/**
 * 
 */
package dev.fujioka.wilson.agendamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.fujioka.wilson.agendamento.Agendamento;

/**
 * @author wilson
 *
 */
@Repository
public interface AgendamentoRepository extends JpaRepository<Agendamento, Long> {

}
