/**
 * 
 */
package dev.fujioka.wilson.task;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import dev.fujioka.wilson.categoria.CategoriaTask;
import dev.fujioka.wilson.usuario.Usuario;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author wilson
 *
 */
@Entity
@Getter
@Setter
@Builder
@EqualsAndHashCode
@Table(name = "tarefas")
public class Task implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column
	@NotBlank(message="Titulo é obrigatório")
	private String titulo;
	
	@Column
	@NotBlank(message="Descrição é obrigatório")
	private String descricao;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private LocalDate vigenciaInicial;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private LocalDate vigenciaFinal;
	
	@Column
	private int qtdeRepeticao;
	
	@OneToOne
	@JoinColumn(name = "id_categoria")
	private CategoriaTask categoria;
	
	@ManyToOne
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private LocalDate create_at;
	
	@JsonFormat(pattern="dd-MM-yyyy")
	private LocalDate update_at;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getVigenciaInicial() {
		return vigenciaInicial;
	}

	public void setVigenciaInicial(LocalDate vigenciaInicial) {
		this.vigenciaInicial = vigenciaInicial;
	}

	public LocalDate getVigenciaFinal() {
		return vigenciaFinal;
	}

	public void setVigenciaFinal(LocalDate vigenciaFinal) {
		this.vigenciaFinal = vigenciaFinal;
	}
	
	public int getQtdeRepeticao() {
		return qtdeRepeticao;
	}

	public void setQtdeRepeticao(int qtdeRepeticao) {
		this.qtdeRepeticao = qtdeRepeticao;
	}
	
	
	public CategoriaTask getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaTask categoria) {
		this.categoria = categoria;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public LocalDate getCreate_at() {
		return create_at;
	}

	public void setCreate_at(LocalDate create_at) {
		this.create_at = create_at;
	}

	public LocalDate getUpdate_at() {
		return update_at;
	}

	public void setUpdate_at(LocalDate update_at) {
		this.update_at = update_at;
	}
	
	
}
