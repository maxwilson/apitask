/**
 * 
 */
package dev.fujioka.wilson.task.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dev.fujioka.wilson.categoria.CategoriaTask;
import dev.fujioka.wilson.categoria.repository.CategoriaTaskRepository;
import dev.fujioka.wilson.task.Task;
import dev.fujioka.wilson.task.repository.TaskRepository;
import dev.fujioka.wilson.usuario.Usuario;
import dev.fujioka.wilson.usuario.repository.UsuarioRepository;

/**
 * @author wilson
 *
 */
@Service
public class TaskService {
	
	@Autowired
	private TaskRepository repository;
	
	@Autowired
	private UsuarioRepository usuarior_repository;
	
	@Autowired
	private CategoriaTaskRepository categoria_repository;
	
	
	@Transactional
	public void save(Task task, Long categoria_id, Long usuario_id) {
		Usuario usuario = this.usuarior_repository.getOne(usuario_id);
		CategoriaTask categoria = this.categoria_repository.getOne(categoria_id);
		task.setCategoria(categoria);
		task.setUsuario(usuario);
		this.repository.save(task);
	}
	
	@Transactional
    public void delete(Task task) {
		repository.delete(task);
    }

	@Transactional
    public void deleteById(long id) {
		repository.deleteById(id);
    }
	
	@Transactional(readOnly=true)
	public Optional<Task> getById(Long id) {
		return this.repository.findById(id);
	}
	
	@Transactional
    public long count() {
        return repository.count();
    }
    
	@Transactional(readOnly=true)
	public List<Task> list() {
		return this.repository.findAll();
	}
	
}
