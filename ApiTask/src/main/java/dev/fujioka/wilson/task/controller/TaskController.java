/**
 * 
 */
package dev.fujioka.wilson.task.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import dev.fujioka.wilson.task.Task;
import dev.fujioka.wilson.task.service.TaskService;
import dev.fujioka.wilson.usuario.Usuario;


/**
 * @author wilson
 *
 */
@RestController
@RequestMapping("/api")
public class TaskController {
	
	@Autowired
	private TaskService service;
	

	@Autowired
	public TaskController(TaskService service) {
		this.service = service;
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping("/task")
	public ResponseEntity<Task> save(@RequestBody Task task,@RequestParam(name = "categoria_id") Long categoria_id,
			@RequestParam(name = "usuario_id") Long usuario_id) {
		
		// data local
		LocalDate datalocal = LocalDate.now();
		task.setCreate_at(datalocal);
		
		this.service.save(task,categoria_id,usuario_id);
		return new ResponseEntity<Task>(task, HttpStatus.CREATED);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@GetMapping("/task")
	public ResponseEntity<List<Task>> list() {
		List<Task> tasks = this.service.list();
		return ResponseEntity.ok(tasks);
	}
	
	@ResponseStatus(code = HttpStatus.FOUND)
	@GetMapping("/task/{id}")
	public ResponseEntity<Task> get(@PathVariable Long id){
		
		Optional<Task> taskOptional = this.service.getById(id);
		
		if(taskOptional.isPresent()) {
			Task task = taskOptional.get();
			return ResponseEntity.ok().body(task);
		}
		return ResponseEntity.noContent().build();
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@DeleteMapping("/task/{id}")
	public ResponseEntity<Task> delete(@PathVariable Long id) {
		Optional<Task> task = this.service.getById(id);
		if (task.isPresent()) {
			this.service.deleteById((task.get().getId()));
			return ResponseEntity.ok().body(task.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping("/task")
	public ResponseEntity<Task> edit(@RequestBody Task task,@RequestParam(name = "categoria_id") Long categoria_id,
			@RequestParam(name = "usuario_id") Long usuario_id){
		
		Optional<Task> taskOptional = this.service.getById(task.getId());
		Task updateTask = taskOptional.get();
		
		if(taskOptional.isPresent()) {
			// data local
			LocalDate datalocal = LocalDate.now();
			updateTask.setTitulo(task.getTitulo());
			updateTask.setDescricao(task.getDescricao());
			updateTask.setQtdeRepeticao(task.getQtdeRepeticao());
			updateTask.setVigenciaInicial(task.getVigenciaInicial());
			updateTask.setVigenciaFinal(task.getVigenciaFinal());
			updateTask.setUpdate_at(datalocal);
			this.service.save(updateTask,categoria_id,usuario_id);		
			return ResponseEntity.ok(updateTask);
		}else {
			return ResponseEntity.noContent().build();
		}
		
	}
}
