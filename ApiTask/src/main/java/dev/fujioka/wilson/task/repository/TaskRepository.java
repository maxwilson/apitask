/**
 * 
 */
package dev.fujioka.wilson.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dev.fujioka.wilson.task.Task;

/**
 * @author wilson
 *
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

}
